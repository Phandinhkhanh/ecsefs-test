FROM nginx:1.11.5
# Test nginx for the EFS ECS
ADD startup.sh .
RUN chmod +x startup.sh
CMD ["./startup.sh"]